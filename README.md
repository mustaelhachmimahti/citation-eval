# Citations

## Get Started

1. `npm i` Installs both sides dependencies (client, server)
2. copy `.env.example` file to `.env` and complete it
3. `admin` password is `adminadmin`
4. `npm run server`
5. `npm run client` (different terminal)
6. user `admin` password is `adminadmin`


## Available Scripts

In the project directory, you can run:

### `npm run client`

Runs the React font-end app in the development mode.

### `npm run server`

Runs the Express back-end server in the development mode.

### `npm run build`

Builds the React font-end app for production to the `build` folder.\
