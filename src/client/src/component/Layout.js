import React from 'react'
import Header from "./Header";
import Footer from "./Footer";
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import LoginHeader from "./LoginHeader";


const Layout = (props) => {
    return (
        <>
            <Router>
                <Routes>
                    <Route path="/*" element={<Header/>}/>
                    <Route path="/login" element={<LoginHeader/>}/>
                    <Route path="/signup" element={<LoginHeader/>}/>
                </Routes>
                {props.children}
                <Footer/>
            </Router>
        </>
    )
}

export default Layout