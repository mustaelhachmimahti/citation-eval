import React, {useEffect, useState} from 'react'
import axios from "axios";
import signup from "./Signup";

const Citations = (props) => {
const [citations, setCitation] = useState([])
    useEffect(() => {
        axios.post('/citations/citations-by-author', {id: localStorage.getItem('id_author')}).then((response) => {
            setCitation(response.data);
        });
    }, []);
    return (
        <>
            <h1 className="card-title">{props.title.toUpperCase()} de {citations[0] !== undefined ? citations[0].full_name : "l'auteur"}</h1>
            <div className="all-cards">
                {citations.map(e => (
                    <div className="card" key={e.id}>
                        <div className="container">
                            <p>{e.citation}</p>
                        </div>
                    </div>
                ))}
            </div>
        </>
    )
}

export default Citations