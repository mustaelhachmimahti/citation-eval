import React from 'react'
import logo from '../logo.png'
import {Link} from 'react-router-dom'

const LoginHeader = () => {
    return (
        <header>
            <nav className="navbar">
                <div className="logo"><img src={logo} className="App-logo" alt="logo" /> <p>Citation.fr</p></div>
                <ul className="nav-links">
                    <input type="checkbox" id="checkbox_toggle"/>
                    <label htmlFor="checkbox_toggle" className="hamburger">&#9776;</label>
                    <div className="menu">
                        <li><Link to="/login">Login</Link></li>
                        <li><Link to="/signup">Sign Up</Link></li>
                    </div>
                </ul>
            </nav>

        </header>
    )
}

export default LoginHeader