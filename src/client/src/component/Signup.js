import React, {useEffect, useState} from 'react';
import axios from "axios";
import login from "./Login";

const Signup = () => {
    const url_string = window.location.href
    const url = new URL(url_string);
    const params = url.searchParams.get("value");
    const close = (e) => {
        const alert = document.querySelector('.alert')
        alert.style.display = "none";
    }
    const [signFormData, setSignFormData] = useState({
        email: '',
        username: '',
        password: '',
        confirm_password: '',
    })

    const handleSubmit = async () => {
        if(!signFormData.email.length){
            const string = encodeURIComponent("Email are required");
            window.location = `/signup?value=${string}`
        }
        if(!signFormData.username.length){
            const string = encodeURIComponent("Username are required");
            window.location = `/signup?value=${string}`
        }if(!signFormData.password.length){
            const string = encodeURIComponent("Password are required");
            window.location = `/signup?value=${string}`
        }if(!signFormData.confirm_password.length){
            const string = encodeURIComponent("Confirm password are required");
            window.location = `/signup?value=${string}`
        }
        if(signFormData.password !== signFormData.confirm_password){
            const string = encodeURIComponent("Confirm password and password are not the same");
            window.location = `/signup?value=${string}`
        }
        if(signFormData.password.length < 6){
            const string = encodeURIComponent("Password must be at least 6 characters");
            window.location = `/signup?value=${string}`
        }
        try{
            const res = await axios.post('/auth/signup', signFormData)
            const {affectedRows} = res.data
            if(affectedRows){
                window.location = '/login'
            }
        }catch (e){
            const string = encodeURIComponent("There are a user with same email or username");
            window.location = `/signup?value=${string}`
        }
    }
    return (
        <>
            {
                params ? <div className="alert">
                    <span className="closebtn" onClick={close}>&times;</span>
                    <strong>Error</strong> {params}
                </div> : null
            }
            <h1 className="login-title">Sign Up</h1>
            <form>
                <div className="container">
                    <label htmlFor="uname"><b>Email</b></label>
                    <input type="email" placeholder="Enter Email" onChange={e =>
                        setSignFormData({
                            ...signFormData,
                            email: e.target.value,
                        })
                    } required autoComplete="off"/>

                    <label htmlFor="psw"><b>Username</b></label>
                    <input type="text" placeholder="Enter Username" onChange={e =>
                        setSignFormData({
                            ...signFormData,
                            username: e.target.value,
                        })
                    } required autoComplete="off"/>
                    <label htmlFor="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" onChange={e =>
                        setSignFormData({
                            ...signFormData,
                            password: e.target.value,
                        })
                    } required autoComplete="off"/>
                    <label htmlFor="psw"><b>Password</b></label>
                    <input type="password" placeholder="Confirm Password" onChange={e =>
                        setSignFormData({
                            ...signFormData,
                            confirm_password: e.target.value,
                        })
                    } required autoComplete="off"/>

                    <button type="button" className="login-button" onClick={handleSubmit}>Login</button>
                </div>
                <div className="container-login">
                    <span className="psw">Forgot <a href="#">password?</a></span>
                </div>
            </form>
        </>
    )
}

export default Signup