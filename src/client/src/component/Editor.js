import React, {useState} from "react";
import axios from "axios";

const Editor = () => {
    const [edit, setEdit] = useState({
        author: localStorage.getItem('full_name'),
        citation: localStorage.getItem('citation')
    })

const makeModify = async () => {
    console.log(edit.author)
        const dataToSend = {
            ...edit, id: localStorage.getItem('id'),
            id_author:localStorage.getItem('id_author')
        }

    const data = await axios.post('/citations/modify', dataToSend )
    if(data.status === 200){
        window.location.reload()
    }

}
    return(
        <div className="edit-container">
            <h1 className="edit-title">Modifier</h1>
            <form>
            <input type='text' value={edit.author} onChange={e =>
                setEdit({
                    ...edit,
                    author: e.target.value,
                })
            }/>
            <input type='text' value={edit.citation} onChange={e =>
                setEdit({
                    ...edit,
                    citation: e.target.value,
                })
            }/>
            <button type="button" onClick={makeModify}>Save</button>
            </form>
        </div>
    )

}

export default Editor