import React, {useEffect, useState} from 'react'
import {Link} from "react-router-dom";
import axios from "axios";

const Card = (props) => {
    const [options, setOptions] = useState(false)
    const getNumberCitation= async (e)=> {
        localStorage.setItem('id_author', e.target.value)
    }
    useEffect(() => {

        if (localStorage.getItem('auth')) {
            if (JSON.parse(localStorage.getItem('auth')).data.role_admin) {
                setOptions(true)
            }
        }
    })
    const deleteAuthor = async (e) => {
        const res = await axios.post('/authors/delete-author', {id: e.target.value})
        window.location.reload()
    }
    const deleteCitation = async (e) => {
        const res = await axios.post('/citations/delete-citation', {id: e.target.value})
        window.location.reload()
    }

    return (
        <>
            <h1 className="card-title">{props.title.toUpperCase()}</h1>
            <div className="all-cards">
                {
                    props.data.currentCitations !== undefined ?
                        props.data.currentCitations.map(e => (
                            <div className="card" key={e.id}>
                                <div className="container">
                                    <p>{e.citation}</p>
                                </div>
                                <div>
                                    {options ?<button value={e.id} onClick={deleteCitation}>Delete</button>: null}
                                </div>
                            </div>
                        )) :
                        props.data.author.map(e => (
                            <div className="card" key={e.id_author}>
                                <div className="container">
                                    <p>{e.full_name}</p>
                                </div>
                                <div>
                                    {options ?<button value={e.id_author} onClick={deleteAuthor}>Delete</button>: null}
                                <Link to='/cita'><button value={e.id_author} onClick={getNumberCitation}>
                                    Voir</button></Link>
                                </div>
                            </div>
                        ))
                }
            </div>
        </>
    )
}

export default Card