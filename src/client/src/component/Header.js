import React, {useState} from 'react'
import logo from '../logo.png'
import {Link} from 'react-router-dom'

const Header = () => {
    const [refresh]= useState(3600000)
    const handleLogOut = () => {
        localStorage.removeItem('auth');
        window.location.reload()
    }
    // For each hour it will log out
    setTimeout(handleLogOut, refresh)

    return (
        <header>
            <nav className="navbar">
                <div className="logo"><img src={logo} className="App-logo" alt="logo" /> <p>Citation.fr</p></div>
                <ul className="nav-links">
                    <input type="checkbox" id="checkbox_toggle"/>
                    <label htmlFor="checkbox_toggle" className="hamburger">&#9776;</label>
                    <div className="menu">
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/citation">Citation</Link></li>
                        <li className="services">
                            <Link to="/auteurs">Auteurs</Link>
                        </li>
                        <li><Link to="/login" onClick={handleLogOut}>Log out</Link></li>
                    </div>
                </ul>
            </nav>

        </header>
    )
}

export default Header