import React, {useState} from 'react'
import axios from 'axios'

const Login = () => {
    const url_string = window.location.href
    const url = new URL(url_string);
    const params = url.searchParams.get("value");
    const close = (e) => {
        const alert = document.querySelector('.alert')
        alert.style.display = "none";
    }

    const [formData, setFormData] = useState({
        username: '',
        password: ''
    })

    const handleSubmit = async e => {
        try {
            const data = await axios.post('/auth/login', formData)
            const {results} = data.data
            const {auth} = data.data
            const {token} = data.data
            console.log(token)
            if (auth) {
                localStorage.setItem('auth', JSON.stringify({auth: auth, token: token, data: results}));
                window.location = '/'
            }
        } catch (e) {
            const string = encodeURIComponent("Password or username are incorrect");
            window.location = `/login?value=${string}`
        }
    }
    return (
        <>
            {
                params ? <div className="alert">
                    <span className="closebtn" onClick={close}>&times;</span>
                    <strong>Error</strong> {params}.
                </div> : null
            }
            <h1 className="login-title">Login</h1>
            <form>
                <div className="container">
                    <label htmlFor="uname"><b>Username</b></label>
                    <input type="text" placeholder="Enter Username" onChange={e =>
                        setFormData({
                            ...formData,
                            username: e.target.value,
                        })
                    } required autoComplete="off"/>

                    <label htmlFor="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" onChange={e =>
                        setFormData({
                            ...formData,
                            password: e.target.value,
                        })
                    } required autoComplete="off"/>

                    <button type="button" className="login-button" onClick={handleSubmit}>Login</button>
                </div>
                <div className="container-login">
                    <span className="psw">Forgot <a href="#">password?</a></span>
                </div>
            </form>
        </>
    )
}

export default Login