import React, {useEffect, useState} from 'react'
import axios from "axios";
import {Link} from "react-router-dom";

const Table = (props) => {
    const [options, setOptions] = useState(false)
    useEffect(() => {
        if (localStorage.getItem('auth')) {
            if (JSON.parse(localStorage.getItem('auth')).data.role_admin) {
                setOptions(true)
            }
        }
    })
    const handleModify = async (e) => {
        localStorage.setItem('id', JSON.parse(e.target.value).id)
        localStorage.setItem('id_author', JSON.parse(e.target.value).id_author)
        const res = await axios.post('/citations/unique-citation-author', {id:JSON.parse(e.target.value).id, id_author:JSON.parse(e.target.value).id_author })

        localStorage.setItem('full_name', res.data[0].full_name)
        localStorage.setItem('citation', res.data[0].citation)
    }

    return (
        <table>
            <thead>
            <tr>
                <th>Citations</th>
                <th>Auteurs</th>
                {options ? <th>Options</th>: null}
            </tr>
            </thead>
            <tbody>
            {
                props.data.both.sort().map(e => (
                    <tr key={e.id}>
                        <td>{e.full_name}</td>
                        <td>{e.citation}</td>
                        { options ? <td>
                            <Link to="/edit"><button value={JSON.stringify({id: e.id, id_author: e.id_author })} onClick={handleModify}>Modifier</button></Link>
                        </td> : null}
                    </tr>
                ))
            }
            </tbody>
        </table>
    )
}

export default Table