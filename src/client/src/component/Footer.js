import React from 'react'

const Footer = () => {
    return (
        <footer>
            <p className="copyright">©copyright 2022 citation.fr</p>
        </footer>
    )
}

export default Footer