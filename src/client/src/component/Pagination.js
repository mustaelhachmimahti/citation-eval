import React from 'react'

const Pagination = ({citationPrePage, totalCitations, paginate}) => {
    const pageNumber = []
    for (let i = 1; i <= Math.ceil(totalCitations / citationPrePage); i++) {
        pageNumber.push(i)
    }
    return (
        <nav className="page">
            <div className="pagination">
                <a href="#">&laquo;</a>
                {
                    pageNumber.map(e => (
                        <a onClick={() => paginate(e)} href="#" key={e}>{e}</a>
                    ))
                }
                <a href="#">&raquo;</a>
            </div>
        </nav>
    )

}

export default Pagination