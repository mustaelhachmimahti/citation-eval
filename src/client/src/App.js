import React, {useState, useEffect} from 'react'
import axios from 'axios'
import './App.css';
import Table from './component/Table'
import { Route, Routes, Navigate} from 'react-router-dom'
import Layout from "./component/Layout";
import Card from "./component/Card";
import Pagination from "./component/Pagination";
import Login from "./component/Login";
import Signup from "./component/Signup";
import Citations from "./component/Citations";
import Editor from "./component/Editor";


const App = () => {
    const [citation, setCitation] = useState([])
    const [author, setAuthor] = useState([])
    const [both, setBoth] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [citationPrePage] = useState(10)

    const [auth, setAuth] = useState(false)

    useEffect(() => {
        if (localStorage.getItem('auth')) {
            if (localStorage.getItem('auth')) {
                setAuth(true)
            }
        }
    })

    useEffect(() => {
        async function fetchData() {
            const citation = await axios.get('/citations')
            const citationData = citation.data
            setCitation(citationData)
            const author = await axios.get('/authors')
            const authorData = author.data
            setAuthor(authorData)
            const both = await axios.get('/citations/author-citation')
            const bothData = both.data
            setBoth(bothData)
        }
        fetchData();
    }, [])
    const indexOfLastCitation = currentPage * citationPrePage
    const indexOfFirstCitation = indexOfLastCitation - citationPrePage
    const currentCitations = citation.slice(indexOfFirstCitation, indexOfLastCitation)

    //Change la page
    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    return (

        <Layout className="App">
            <main>
                <Routes>
                    <Route path="/" element={!auth ? <Navigate to="/login" /> :<Table data={{both}}/> }/>
                    <Route path="/citation"
                           element={ !auth ? <Navigate to="/login" /> : <div><Card data={{currentCitations}} title={'Citations'}/><Pagination
                               citationPrePage={citationPrePage} totalCitations={citation.length}
                               paginate={paginate}/></div>}/>
                    <Route path="/auteurs" element={!auth ? <Navigate to="/login" /> :<Card data={{author}} title={'Auteurs'}/>}/>
                    <Route path="/login" element={auth ? <Navigate to="/" /> : <Login/>}/>
                    <Route path="/signup" element={auth ? <Navigate to="/" /> : <Signup/>}/>
                    <Route  path="/cita" element={!auth ? <Navigate to="/login" /> :<Citations title={'Citations'}/>}/>
                    <Route  path="/edit" element={!auth ? <Navigate to="/login" /> :<Editor title={'Citations'}/>}/>
                </Routes>
            </main>
        </Layout>

    );
}

export default App;
