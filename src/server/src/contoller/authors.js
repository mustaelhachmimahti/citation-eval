const express = require('express');
const executeQuery = require('../database')
const routes = express.Router()

routes.get('/' ,async (req, res) => {
    const data = await executeQuery('SELECT * FROM authors', [])
    res.send(data)
})

routes.post('/delete-author', async (req, res) => {
    const data = await executeQuery("DELETE FROM authors WHERE authors.id_author = ?", [req.body.id])
    const data2 = await executeQuery("DELETE FROM citations WHERE citations.id_author = ?", [req.body.id])
    res.send({...data, data2})
})

module.exports = routes