const express = require('express');
const executeQuery = require('../database')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config()

const routes = express.Router()

routes.post('/login', async (req, res) => {
    try {
        const {body} = req
        const response = await executeQuery('SELECT * FROM users WHERE username = ?', [body.username])
        if(!response.length ) {

            const string = encodeURIComponent("Password or username are incorrect");
            res.status(403).redirect(`/login?value=${string}`)
        }else{
            const {password} = response[0]
            const isMatch = await bcrypt.compare(body.password, password);
            if (isMatch) {
                const {id} = response[0].id
                const token = jwt.sign({id}, process.env.SECRET_JWT,{
                    expiresIn: "1h",
                });
                res.status(200).send({auth: true, token: token, results: response[0]})
            } else if (!isMatch) {
                res.status(403).send({auth: false, msg: "Password or username are incorrect"})
            } else {
                res.status(204).send({auth: false, msg:"Ups! Something went wrong"})
            }
        }
    } catch (e) {
        throw new Error(e)
        const string = encodeURIComponent("Try it later please!");
        res.status(500).redirect(`/login?value=${string}`)
    }
})

routes.post('/signup', async (req, res) =>{
   try{
       const {body} = req
       const saltOrRounds = 10;
       const hash = await bcrypt.hash(body.password, saltOrRounds);
       const response = await executeQuery('INSERT INTO users (email, username, password) VALUES (?, ?, ?);', [body.email,body.username, hash])
       res.send(response)
   }catch (e) {
       res.status(404).send(false)
   }
})

module.exports = routes