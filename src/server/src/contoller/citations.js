const express = require('express');
const verifyJWT = require('../services/authService')
const executeQuery = require('../database')

const routes = express.Router()

routes.get('/', async (req, res) => {
    const data = await executeQuery('SELECT * FROM citations', [])
    res.send(data)
})

routes.post('/unique-citation-author', async (req, res) => {
    const {body} = req
    const data = await executeQuery('SELECT * FROM authors JOIN citations ON citations.id_author = authors.id_author WHERE authors.id_author = ? AND citations.id = ?', [body.id_author, body.id])
    res.send(data)
})

routes.get('/author-citation', async (req, res) => {
    const data = await executeQuery('SELECT * FROM authors JOIN citations ON citations.id_author = authors.id_author', [])
    res.send(data)
})

routes.post('/citations-by-author', async (req, res) => {
    const data = await executeQuery('SELECT * FROM citations JOIN authors ON citations.id_author = authors.id_author WHERE authors.id_author = ?', [req.body.id])
    res.send(data)
})
routes.post('/delete-citation', async (req, res) => {
    const data = await executeQuery("DELETE FROM citations WHERE citations.id = ?", [req.body.id])
    res.send(data)
})

routes.post('/modify', async (req, res) => {
    const {body} = req
    const data = await executeQuery("UPDATE citations SET citation = ? WHERE citations.id = ?", [body.citation, body.id])
    const data2 = await executeQuery("UPDATE authors SET full_name = ? WHERE authors.id_author = ?", [body.author, body.id_author])
    res.send({...data, data})
})

module.exports = routes