const jwt = require('jsonwebtoken');
require('dotenv').config()


const verifyJWT = (req, res, next) => {
    const authHeader = req.headers.authorization
    if(authHeader){
        const token = authHeader.split(" ")[1]
        jwt.verify(token, process.env.SECRET_JWT, (err, payload)=> {
            if(err){
                res.status(403).send("Token is not valid")
                return
            }
            req.payload = payload
            next()
        })
    }else{
        res.status(404).send('Not authenticated')
    }
}

module.exports = verifyJWT