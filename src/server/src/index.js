const express = require('express');
const bodyParser = require('body-parser');
const path = require('path')
const cors = require('cors')
require('dotenv').config()

const app = express()

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.set('port', process.env.PORT || 3001)

app.use('/auth', require('./contoller/auth'))
app.use('/citations', require('./contoller/citations'))
app.use('/authors', require('./contoller/authors'))


app.listen(app.get('port'), () => {
    console.log('Server listening on port', app.get('port'))
})