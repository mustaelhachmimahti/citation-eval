-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 23, 2022 at 04:58 PM
-- Server version: 8.0.28-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `citation_eval`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id_author` int NOT NULL,
  `full_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id_author`, `full_name`) VALUES
(2, 'Anass Sakkakii'),
(30, 'Raphael Nadal'),
(31, 'Mateo NAzaro'),
(32, 'Musta Delyan');

-- --------------------------------------------------------

--
-- Table structure for table `citations`
--

CREATE TABLE `citations` (
  `id` int NOT NULL,
  `citation` varchar(255) NOT NULL,
  `id_author` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `citations`
--

INSERT INTO `citations` (`id`, `citation`, `id_author`, `created_at`) VALUES
(9, 'Citation 4', 2, '2022-02-23 14:44:15'),
(10, 'Citation 2', 2, '2022-02-23 14:44:54'),
(11, 'Citation 3', 2, '2022-02-23 14:45:06'),
(12, 'Citation 10', 30, '2022-02-23 14:45:20'),
(13, 'Citation 7', 30, '2022-02-23 14:45:31'),
(14, 'Citation 6', 30, '2022-02-23 14:45:35'),
(15, 'Citation 8', 2, '2022-02-23 14:45:43'),
(16, 'Citation 5', 31, '2022-02-23 14:46:42'),
(17, 'Citation 11', 31, '2022-02-23 14:46:46'),
(18, 'Citation 12', 31, '2022-02-23 14:46:55'),
(19, 'Citation 12', 30, '2022-02-23 14:46:59'),
(20, 'Citation 16', 2, '2022-02-23 14:47:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `role_admin`) VALUES
(2, 'musta@musta.com', 'musta', '$2a$12$loR26aTYITlifW/jaBJIMuzblPy7re4FhFFeSmxouwT1yaH6Kzmai', 0),
(30, 'admin@admin', 'admin', '$2b$10$jRDpuN5KhFvowcto9e//Su9uBmCfeidbKnxwzwc5EK9siqTvzMf.e', 1),
(31, 'dani@dani.com', 'dani', '$2a$12$VEUcxRcHFh40g6AInUIL6.uTBg28Qk8msV3I9BjXNDxjQSVZQXEsy', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id_author`);

--
-- Indexes for table `citations`
--
ALTER TABLE `citations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id_author` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `citations`
--
ALTER TABLE `citations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
